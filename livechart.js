/**
 * 
 * 
 */


function makeRecipeObject(refRecipe) {
    let check = SatisfactoryData.recipesGraph.get( refRecipe[ 'className' ] );
    if( check ) {
        return check;
    }

    //console.log( 'refrecipe:', refRecipe );
    let self = { 
        'ref': refRecipe,
        'className': refRecipe[ 'className' ],
        'name': refRecipe[ 'slug' ]
    };

    self.getMyProducts = function( onlyHigherTier = false ) {
        const result = [];
        const myTier = this.getMyTier();

        self.ref.products.forEach( function( item ) {
            const obj = SatisfactoryData.itemsGraph.get( item.item );

            if( onlyHigherTier && obj.getMyTier() < myTier ) {
                return;
            }

            result[ result.length ] = obj;
        });

        return result;
    },

    self.getMyIngredients = function() {
        const result = [];

        self.ref.ingredients.forEach( function( item ) {
            const obj = SatisfactoryData.itemsGraph.get( item.item );

            result[ result.length ] = obj;
        });

        return result;
    }

    self.getMyTier = function (stack) {

        if( null == stack ) stack = [];

        let maxT = 0; 
        let indent = new Array( stack.length + 1 ).join( '\t' );

        if( this['ref']['ingredients'] ) {
            maxT = 1;
        }

        for (i in this['ref']['ingredients']) {
            let className = this['ref']['ingredients'][i]['item'];

            if ( stack.includes( className ) ) {
                console.log( indent + 'skipping loop for ' + className + '...');
                continue;
            }

            let tmp = SatisfactoryData.itemsGraph.get( className );
            if (null == tmp) {
                console.log( 'adding ingredients to graph:' + className );
                tmp = makeItemObject(className, null, true );
            }

            let ingTier = tmp.getMyTier(stack);
            if (maxT < (ingTier + 1)) {
                maxT = ingTier + 1;
            }

        }

        // console.log('recipe ' + this.getName() + ' has tier:' + maxT);

        return maxT;
    }
    

    SatisfactoryData.recipesGraph.set( self.className, self );

    if( refRecipe.inMachine ) {

        // console.log( 'ingredients: ', refRecipe[ 'ingredients' ] );

        for( i in refRecipe[ 'ingredients' ] ) {
            let className = refRecipe[ 'ingredients' ][ i ][ 'item' ];

            if( null == SatisfactoryData.itemsGraph.get( className ) ) {
                //console.log( 'make from ingredient ' + className );
                makeItemObject( className, null, true );
            } else {
                //console.log( 'ing already exist.' );
            }
            
        }

        // console.log( 'products: ', refRecipe[ 'products' ] );
        for( i in refRecipe[ 'products' ] ) {
            let className = refRecipe[ 'products' ][ i ][ 'item' ];

            makeItemObject( className, self, true );
        }
   
        if( refRecipe.alternate ) {
            SatisfactoryData.alternateRecipes[ SatisfactoryData.alternateRecipes.length ] = self.className;
        } else {
            SatisfactoryData.normalRecipes[ SatisfactoryData.normalRecipes.length ] = self.className;
        }

        refRecipe.producedIn.forEach(element => {
            if( null == SatisfactoryData.machines[ element ] ) {
                SatisfactoryData.machines[ element ] = [];                
            }

            SatisfactoryData.machines[ element ] [SatisfactoryData.machines[ element ].length ] = self.className;
        });
    }

    if( refRecipe.forBuilding ) {
        SatisfactoryData.buildingParts[ SatisfactoryData.buildingParts.length ] = self.className;
    }

    // console.log( 'added recipe: ', self, 'ref:', refRecipe );

    return self;
}


function makeItemObject( className, recipeObj, muteable) {
    const ref = SatisfactoryData.gameData['items'][ className ];
    // console.log( className, ' item ref:', ref );
    
    let obj = SatisfactoryData.itemsGraph.get(className);
    if (null == obj) {
        obj = {
            'ref': ref,
            'name': ref[ 'name' ],
            'className': ref[ 'className' ],
            'recipes': {},
            'muteable': muteable,
            'tier': null,
            'ingredients': null,

            'addRecipe': function (recipeObj) {
                this.recipes[recipeObj.className] = recipeObj;
                if (this.muteable) {
                    this.tier = null;
                    this.ingredients = null;
                }
                //this.getMyTier();
            },

            'getIngredients': function() {
                // console.log( this.name + ' ingredient is:', this.ingredients );
                if( null == this.ingredients ) {
                    this.ingredients = [];
                    for (i in this.recipes) {
                        // console.log( '   in rec: ', this.recipes[ i ] );
                        for( j in this.recipes[ i ].ref.ingredients ) {
                            const ingredClassName = this.recipes[ i ].ref.ingredients[ j ].item;
                            // console.log( '   item: ', ingredClassName );
                            if( ! this.ingredients.includes( ingredClassName ) ) {
                                this.ingredients[ this.ingredients.length ] = ingredClassName;
                            }
                        }
                    }
                }

                return this.ingredients;
            },

            'getMyTier': function (stack) {

                if (null == this.tier) {
                    const myStack = ( null == stack ? [] : stack.slice() );

                    let indent = new Array( myStack.length + 1 ).join( '\t' );
                    let minT = 0;
                    let first = true;
                
                    console.log( indent + "getting tier for " + this.name );
                    myStack[myStack.length] = this.className                   ;
                    // console.log( 'set whoisasking' );

                    for (i in this.recipes) {
                        // console.log( 'recipe: ', this.recipes[i] );
                        let rName = this.recipes[i].name;
                        // console.log( indent + " checking recipe: " + rName);
                        let tmp = this.recipes[i].getMyTier(myStack);
                        console.log( indent + ' product ' + this.className
                         + ' checking recipe: ' + rName + ' has tier:' + tmp);

                        if (first) {
                            minT = tmp;
                            first = false;
                            continue;
                        }


                        if (tmp < minT) {
                            minT = tmp;
                        }

                        // console.log( 'product ' + this.name + ' after recipe: ' + rName + ' minT is ' + minT );
                    }

                    this.tier = minT;
                    console.log( indent + 'product ' + this.className + ' has tier: ' + this.tier );
                }              

                return this.tier;
            },

        };

        SatisfactoryData.itemsGraph.set(className, obj);
    }

    if (recipeObj) {
        obj.addRecipe( recipeObj );
    }

    // sconsole.log('made:', obj);

    return obj;
}


function makeSatisfactoryData( ) {



    const self = {
        'itemsGraph': new Map(),
        'recipesGraph': new Map(),
        'normalRecipes': [],
        'alternateRecipes': [],
        'machines': {},
        'buildingParts': [],
        'gameData':{},

        'machinesOrder': [
            'BUild_Mine_general_C',
            'Desc_SmelterMk1_C',
            'Desc_ConstructorMk1_C',
            'Desc_FoundryMk1_C',
            'Desc_AssemblerMk1_C',
            'Desc_ManufacturerMk1_C',
            'Desc_OilRefinery_C',
            'Desc_Blender_C',
            'Desc_HadronCollider_C',
            'Desc_Packager_C',         
        ],

        'initItemGraph': initItemGraph,
        getRecipesIterable: function() {
            return {
                [Symbol.iterator]: getRecipesIterator
            };
        },
        getPartsIterable: function() {
            console.log( 'partsiterable was called');
            return {
                [Symbol.iterator]: getPartsIterator
            }
        }
        
    };


    function getPartsIterator( uniqe = true ) {
        const tmp = {
            _history: new Set(),
            _recipesIterator: getRecipesIterator(),
            _itemsList: null,
            _itemsIndex: 0,

            next: function() {
                const result = {
                    done: true,
                    value: null
                };

                if( this._itemsList && !( this._itemsIndex < this._itemsList.length ) ) {
                    this._itemsList = null;
                }

                if( null == this._itemsList ) {
                    const itrRet = this._recipesIterator.next();
                    if( ! itrRet.done ) {

                        this._itemsList = itrRet.value.getMyIngredients();
                        this._itemsList = this._itemsList.concat( itrRet.value.getMyProducts( true ) );
                        this._itemsIndex = 0;

                        if( 0 == this._itemsList.length ) {
                            return this.next();
                        }
                    }
                }

                if( this._itemsList && this._itemsIndex < this._itemsList.length ) {
                    result.done = false;
                    result.value = this._itemsList[ this._itemsIndex ];
                    this._itemsIndex += 1;
                    
                    if( uniqe ) {
                        if( this._history.has( result.value ) ) {
                            // console.log( 'skipping duplicate part ', result.value );
                            return this.next();
                        } else {
                            this._history.add( result.value );
                        }
                    }                    
                }
                

                // console.log( 'parts next:', result );
                return result;
            }
        };

        return tmp;
    }


    function getRecipesIterator() {
        const tmp = {
            _machineIndex : 0,
            _machineLength: self.machinesOrder.length,
            _listIndex: 0,
            _listLength: self.machines[ self.machinesOrder[ 0 ] ].length,

            next: function() {

                let result = null;

                if( ! ( this._listIndex < this._listLength ) ) {

                    this._machineIndex += 1;
                    if( this._machineIndex < this._machineLength ) {
                        this._listIndex = 0;
                        this._listLength = self.machines[ self.machinesOrder[ this._machineIndex ] ].length;
                    }
                  
                }
                
                if( this._machineIndex < this._machineLength && this._listIndex < this._listLength ) {
                    result = self.machines[ self.machinesOrder[ this._machineIndex ] ][ this._listIndex ];
                    this._listIndex += 1;
                }

                // console.log( 'iterator: ', this );        

                return {
                    done: result ? false : true,
                    value: result ? self.recipesGraph.get( result ) : null
                };
            }
        };

        // console.log( 'iterator: ', tmp );
        return tmp;
    }


    function initItemGraph() {

        for ( const i in self.gameData["resources"] ) {

            const mockRecipe = {
                "slug": 'mine-' + i,
                "name": 'mine for ' + i,
                "className": 'mine_' + i,
                "alternate": false,
                "inMachine": true,
                "ingredients": [],
                "products": [{
					"item": i,
					"amount": 60
				}],
                "producedIn": [
                    "BUild_Mine_general_C"
                ]
            };

            const mockRecipeObj = makeRecipeObject( mockRecipe );
            
            mockRecipeObj.getMyTier();        
        }

        for (i in self.gameData["recipes"]) {
            const tmpobj = self.gameData["recipes"][i];

            makeRecipeObject( tmpobj );            
        }

        console.log( '================ finished adding parts ====================' );
        
        for( v of self.getPartsIterable() ) {
            v.getMyTier();
            console.log( 'part: ', v  );
        }
    }

    return self;
}


const SatisfactoryData = makeSatisfactoryData();


function generateGraph() {
    SatisfactoryData.initItemGraph();
    console.log( SatisfactoryData );


    let graph = Viva.Graph.graph();

    for( v of SatisfactoryData.getPartsIterable() ) {
        graph.addNode( v.className, v );
    }


    for( v of SatisfactoryData.getRecipesIterable() ) {
        // console.log( 'recipe: ', v );
        const ings = v.getMyIngredients();
        const prods = v.getMyProducts( true );
        
        for ( i of ings ) {
            
            for( j of prods ) {
                // console.log( 'adding link from ', i, ' to ', j, 'via: ', v );
                graph.addLink( i.className, j.className, v);
            }
        }
    }

    return graph;    
}

function generateGraphics() {
    const graphics = Viva.Graph.View.svgGraphics();

    // Nothing changed in these lines. They are the same as in Step 2
    // of this tutorial. Except maybe chaining support:
    graphics.node(function (node) {
        // console.log(node);

        let name = node.id;
        let tier = -1;

        if( node.data ) {
            name = node.data.name;
            tier = node.data.getMyTier();
        }

        return Viva.Graph.svg('text')
            .attr('width', 24)
            .attr('height', 24)
            // .link('' + node.data);
            .text( name + '@' + tier );
        ;
    }).placeNode(function (nodeUI, pos) {
        // console.log('nodeUI', nodeUI);
        var tier = nodeUI.node.data ? nodeUI.node.data.getMyTier() : 10;

        // pos.x = ( pos.x * 2 ) + 20;
        // pos.y = (tier * 60 ) + 2;

        //nodeUI.attr('x', ( pos.x * 2 ) + 20).attr('y', 2 + (tier * 60));
        nodeUI.attr('x', pos.x ).attr('y', pos.y );
    });

    // Step 4. Customize link appearance:
    //   As you might have guessed already the link()/placeLink()
    //   functions complement the node()/placeNode() functions
    //   and let us override default presentation of edges:
    graphics.link(function (link) {
        return Viva.Graph.svg('path')
            .attr('stroke', 'red')
            .attr('stroke-dasharray', '5, 5');
    }).placeLink(function (linkUI, fromPos, toPos) {
        // linkUI - is the object returend from link() callback above.
        // console.log( 'linkUI:', linkUI );
        var data = 'M' + fromPos.x + ',' + fromPos.y +
            'L' + toPos.x + ',' + toPos.y;

        // 'Path data' (http://www.w3.org/TR/SVG/paths.html#DAttribute )
        // is a common way of rendering paths in SVG:
        linkUI.attr("d", data);
        // console.log( "link: ", data );
    });

    return graphics;
}


function main() {

    const graph = generateGraph();
    const graphics = generateGraphics();

     // Step 5. Render the graph with our customized graphics object:
    const renderer = Viva.Graph.View.renderer(graph, {
        container: document.getElementById('graphDiv'),
        graphics: graphics
    });

    renderer.run();

    
    console.log( renderer );
    renderer.pause();

    let thisLayout = renderer.getLayout();
    console.log( 'layout:', thisLayout );
    console.log( 'graph: ', graph );


    thisLayout.forEachBody( function( body, nodeId ) {
        console.log( 'layout body: ', body );
        let node = graph.getNode( nodeId );
        let tier = node.data ? node.data.getMyTier() : -1;

        console.log( 'node:', node );
        body.pos.x = ( body.pos.x * 2 ) + 20;
        body.pos.y = ( tier * 60 ) + 2;

        body.mass = body.mass + ( body.mass * .1 * tier );
    });


    graph.forEachLink( function( link ) {
        let nodeFrom = graph.getNode( link.fromId );
        let nodeTo = graph.getNode( link.toId );
        let spring = thisLayout.getSpring( link.fromId, link.toId );

        // console.log( 'sp1: ', nodeFrom, nodeTo, spring );

        let fromTier = 0
        if( nodeFrom.data ) { 
            fromTier = nodeFrom.data.getMyTier();
        } else {
            console.log( 'node ', nodeFrom, ' has no data' );
        }

        let toTier = 1;
        if( nodeTo.data ) {
            toTier = nodeTo.data.getMyTier();
        } else {
            console.log( 'node ', nodeTo, ' has no data' );
        }

        let diff = Math.abs( toTier - fromTier ) + 1;

        spring.length = Math.pow( diff, 2 ) * 2;
        spring.weight /= diff;
        spring.coeff = - 0.03;

        console.log( 'spring: ', spring );

        return link;
    });

   
    thisLayout.step( 50 );


    renderer.resume();    
}



function nomain() {

    // let graphGenerator = Viva.Graph.generator();
    // let graph = graphGenerator.grid( 21, 11 );
    let graph = Viva.Graph.graph();

    
    graph.addNode(i, itemObj);

    for (i in SatisfactoryData["recipes"]) {
        const tmpobj = SatisfactoryData["recipes"][i];

        if (0 == tmpobj.producedIn.length) {
            // console.log('Skipping ' + tmpobj.name );
            continue;
        }

        if( 1 == tmpobj.producedIn.length &&
            'Desc_Packager_C' == tmpobj.producedIn[ 0 ] ) {
            // skip packager
            continue;
        }

        for( k in tmpobj['products'] ) {

            let className = tmpobj['products'][k]['item'];

            let itemObj = makeItemObject(className, tmpobj, true);

            graph.addNode(className, itemObj);

            //
        }

    }

    console.log(self.itemsGraph);

    for (const [key, value] of self.itemsGraph ) {
        console.log( key + ' at tier:' + value.getMyTier() );
        let links = value.getIngredients();
        console.log( '====  ' + value.name + ' ingredients:', links );

        for( j in links ) {
            // console.log( 'adding link from ' + links[ j ] + ' to ' + value.className );
            graph.addLink( links[j], value.className );
        }
    }


    // return;
    // plot it

  

    // Step 5. Render the graph with our customized graphics object:
    var renderer = Viva.Graph.View.renderer(graph, {
        graphics: graphics
    });

    renderer.run();
    console.log( renderer );
    renderer.pause();

    let thisLayout = renderer.getLayout();
    console.log( 'layout:', thisLayout );
    console.log( 'graph: ', graph );

    

    thisLayout.forEachBody( function( body, nodeId ) {
        // console.log( 'layout body: ', body );
        let node = graph.getNode( nodeId );
        let tier = node.data ? node.data.getMyTier() : -1;

        // console.log( 'node:', node );
        body.pos.x = ( body.pos.x * 2 ) + 20;
        body.pos.y = ( tier * 60 ) + 2;

        body.mass = body.mass + ( body.mass * .1 * tier );
    });


    graph.forEachLink( function( link ) {
        let nodeFrom = graph.getNode( link.fromId );
        let nodeTo = graph.getNode( link.toId );
        let spring = thisLayout.getSpring( link.fromId, link.toId );

        console.log( 'sp1: ', nodeFrom, nodeTo, spring );

        let fromTier = nodeFrom.data.getMyTier();
        let toTier = nodeTo.data.getMyTier();

        spring.length *= Math.abs( toTier - fromTier );

        console.log( 'sp1: ', spring );

    });

    thisLayout.step( 50 );

    renderer.resume();

    /*
    graph.forEachNode( function(node) {
        let pos = thisLayout.getNodePosition( node.id );
        console.log( 'pos:', pos );

        let tier = node.data ? node.data.getMyTier() : -1;

        pos.x = ( pos.x * 2 ) + 20;
        pos.y = ( tier * 60 ) + 2;

        console.log( 'pos:', pos );
        console.log( 'body:', thisLayout.getBody( node.id ) );

        thisLayout.setNodePosition( node.id, pos.x, pos.y );
    });
    */

    // 

}